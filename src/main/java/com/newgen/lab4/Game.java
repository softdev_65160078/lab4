/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.newgen.lab4;

import java.util.Scanner;

/**
 *
 * @author Phattharaphon
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;

    
    public void Game() {
        this.player1 = new Player('O');
        this.player2 = new Player('X');   
    }
    public void newGame() {
        this.table = new Table(player1, player2);
    }
    public void play() {
        showWelcome();
        newGame();
        while(true) {
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()){
                showTable();
//                saveWin();
                showInfo();
                newGame();
            }
            if(table.checkDraw()) {
                showTable();
//                saveDraw();
                showInfo();
                newGame();
            }
            table.switchPlayer();        
            }
       }
//    private void saveWin() {
//        if(player1 == table.getCurrentPlayer()) {
//            player1.win();
//            player2.lose();
//        } else {
//           player2.win();
//           player1.lose();
//        }
//    }
//   private void saveDraw() {
//           player1.draw();
//            player2.draw();
//    }
    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++)
            System.out.println(t[i][j] + " ");
        }
        System.out.println("");
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + "Turn");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Please input row col:");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);        
    }
}
